<?php
/** @noinspection PhpDocSignatureIsNotCompleteInspection */
declare(strict_types=1);

use Robo\Symfony\ConsoleIO;
use Robo\Tasks;

require_once __DIR__."/vendor/autoload.php";

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see https://robo.li/
 */
class RoboFile extends Tasks
{
    /**
     * @return void
     */
    public function test(ConsoleIO $io) : void
    {
        // Task code here...
    }

}

