<?php
declare(strict_types=1);

namespace SpaethTech\REST\Endpoints;

use ReflectionAttribute;
use ReflectionProperty;
use SpaethTech\Reflection\ReflectionCache;
use SpaethTech\REST\Attributes\Delete;
use SpaethTech\REST\Attributes\Get;
use SpaethTech\REST\Attributes\Patch;
use SpaethTech\REST\Attributes\PatchExclude;
use SpaethTech\REST\Attributes\Post;
use SpaethTech\REST\Attributes\PostExclude;
use SpaethTech\REST\RestMethod;

/**
 * EndpointObject
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
abstract class EndpointObject
{
    public function __construct(array $properties = [])
    {
        ReflectionCache::buildCacheForClass(get_called_class());
    }

    public function toJson(int $flags = 0, int $depth = 512) : string
    {
        return json_encode($this->toArray(), $flags, $depth);
    }

    public function toArray(RestMethod $method = RestMethod::GET, array|FALSE &$errors = NULL) : array
    {
        $child = get_called_class();

        if ($errors === NULL)
            $errors = [];

        $output = [];

        $cAttributes = ReflectionCache::getCachedClass($child)["attributes"];

        #region Class Attributes

        // Create
        if ($method === RestMethod::POST &&
            !array_key_exists(Post::class, $cAttributes))
            return [];

        // Read
        if ($method === RestMethod::GET &&
            !array_key_exists(Get::class, $cAttributes))
            return [];

        // Update
        if ($method === RestMethod::PATCH &&
            !array_key_exists(Patch::class, $cAttributes))
            return [];

        // Delete
        if ($method === RestMethod::DELETE &&
            !array_key_exists(Delete::class, $cAttributes))
            return [];

        #endregion

        #region Property Attributes

        foreach(ReflectionCache::getCachedProperties($child) as $property => $cache)
        {
            /** @var ReflectionProperty $pReflection */
            $pReflection = $cache["reflection"];

            #region POST Exclusions

            if (array_key_exists(PostExclude::class, $cache["attributes"]))
            {
                /** @var ReflectionAttribute $pAttribute */
                $pAttribute = $cache["attributes"][PostExclude::class];
                /** @var PostExclude $iAttribute */
                $iAttribute = $pAttribute->newInstance();

                if ($iAttribute->eval == null)
                    continue;

                if (eval("return $iAttribute->eval;"))
                    continue;
            }

            #endregion

            #region PATCH Exclusions

            if (array_key_exists(PatchExclude::class, $cache["attributes"]))
            {
                /** @var ReflectionAttribute $pAttribute */
                $pAttribute = $cache["attributes"][PatchExclude::class];
                /** @var PatchExclude $iAttribute */
                $iAttribute = $pAttribute->newInstance();

                if ($iAttribute->eval == null)
                    continue;

                if (eval("return $iAttribute->eval;"))
                    continue;
            }

            #endregion

            #region Uninitialized

            if (!$pReflection->isInitialized($this))
            {
                // TODO: Determine if we want to handle this differently?

                if (!array_key_exists($property, $errors))
                    $errors[$property] = "Value is not initialized";

                continue;
            }

            #endregion

            $output[$pReflection->getName()] = $pReflection->getValue($this);

        }

        #endregion

        if (count($errors) === 0)
            $errors = FALSE;

        return $output;
    }




    public function __call(string $name, array $arguments)
    {
        if (str_starts_with($name, "get"))
        {
            return $this->{lcfirst(substr($name, 3))};
        }

        if (str_starts_with($name, "set"))
        {
            $this->{lcfirst(substr($name, 3))} = $arguments[0];
            return $this;
        }




    }

//    private function camel2snake(string $input) : string
//    {
//        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
//    }
//    private function snake2camel(string $input) : string
//    {
//        return lcfirst(str_replace(" ", "", ucwords(str_replace("_", " ", $input))));
//    }

}
