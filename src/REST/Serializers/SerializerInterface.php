<?php
declare(strict_types=1);

namespace SpaethTech\REST\Serializers;

/**
 * SerializerInterface
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
interface SerializerInterface
{
    public function serialize(mixed $property) : mixed;

    public function deserialize(mixed $resource) : mixed;

}
