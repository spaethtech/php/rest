<?php
declare(strict_types=1);

namespace SpaethTech\REST\Serializers\Exceptions;

use RuntimeException;

/**
 * SerializationException
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
class SerializationException extends RuntimeException
{
}
