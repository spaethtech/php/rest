<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\REST\Serializers;

use BackedEnum;
use IntBackedEnum;
use SpaethTech\REST\Serializers\Exceptions\SerializationException;
use StringBackedEnum;

/**
 * EnumSerializer
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
class EnumSerializer extends AbstractSerializer
{

    public function __construct(protected BackedEnum $enum)
    {
    }

    public function serialize(mixed $property) : int|string
    {
        if ($property instanceof IntBackedEnum)
            return $property->value;

        if ($property instanceof StringBackedEnum)
            return $property->value;

        if ($property instanceof BackedEnum)
            return $property->value;

        throw new SerializationException();
    }

    public function deserialize(mixed $resource) : StringBackedEnum|IntBackedEnum|BackedEnum
    {

        if ($this->enum instanceof IntBackedEnum && is_int($resource))
        {
            /** @var IntBackedEnum $enum */
            $enum = $this->enum;

            return $enum::from($resource);
        }

        if ($this->enum instanceof StringBackedEnum && is_string($resource))
        {
            /** @var StringBackedEnum $enum */
            $enum = $this->enum;

            return $enum::from($resource);
        }

        if ($this->enum instanceof BackedEnum &&
            (is_int($resource) || is_string($resource)))
        {
            /** @var BackedEnum $enum */
            $enum = $this->enum;

            return $enum::from($resource);
        }

        throw new SerializationException();
    }


}
