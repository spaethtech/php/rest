<?php
declare(strict_types=1);

namespace SpaethTech\REST\Serializers;

/**
 * Serializer
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
abstract class AbstractSerializer implements SerializerInterface
{

}
