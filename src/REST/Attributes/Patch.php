<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\REST\Attributes;

use Attribute;

/**
 * Patch
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
#[Attribute(Attribute::TARGET_CLASS)]
class Patch extends RestAttribute
{
    public function __construct(public string $one)
    {
    }
}
