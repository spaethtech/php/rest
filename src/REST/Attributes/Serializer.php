<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\REST\Attributes;

use Attribute;
use SpaethTech\REST\Serializers\SerializerInterface;

/**
 * Serializer
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_PROPERTY)]
class Serializer extends RestAttribute
{
    /**
     * @param class-string<SerializerInterface> $serializer
     */
    public function __construct(public string $serializer)
    {
    }
}
