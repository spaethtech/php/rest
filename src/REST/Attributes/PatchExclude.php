<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\REST\Attributes;

use Attribute;

/**
 * PatchExclude
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
class PatchExclude extends RestAttribute
{
    public function __construct(public ?string $eval = null)
    {
    }
}
