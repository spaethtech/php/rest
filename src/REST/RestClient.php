<?php
declare(strict_types=1);

namespace SpaethTech\REST;

use GuzzleHttp\ClientInterface;

/**
 * RestClient
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
class RestClient
{
    public function __construct(protected ClientInterface $client)
    {
    }


}
