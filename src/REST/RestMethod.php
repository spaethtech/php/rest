<?php
declare(strict_types=1);

namespace SpaethTech\REST;

/**
 * HttpMethod
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 */
enum RestMethod : string
{
    /** Gets an existing resource */
    case GET        = "GET";

    /** Creates a new resource */
    case POST       = "POST";

    /** Completely replaces an existing resource */
    case PUT        = "PUT";

    /** Updates all or part of an existing resource */
    case PATCH      = "PATCH";

    /** Deletes an existing resource */
    case DELETE     = "DELETE";

    /** Retrieves meta information about an individual resource */
    case HEAD       = "HEAD";

    /** Retrieves meta information about the API */
    case OPTIONS    = "OPTIONS";

}
